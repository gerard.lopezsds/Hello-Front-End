module.exports = {
    entry: ['./Hello.ts'],
    output: {
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                /*
                se puede usar awesome para pasar a em6 y luego babel para pasar a em5 
                y asi cada uno hace una funcion o que lo haga todo el awesome           
                */
                loader: [
                    'awesome-typescript-loader',
                ]
            },
        ]
    }
   
};